/**
 * 
 */
package org.ensg.projetperso;

/**
 * @author Max
 *
 */
public enum CalculGeomEnum {
	Rotation,
	Translation
}
