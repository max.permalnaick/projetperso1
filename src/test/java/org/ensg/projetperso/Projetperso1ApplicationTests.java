package org.ensg.projetperso;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.locationtech.jts.geom.Coordinate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import org.ensg.projetperso.CalculGeom;
import org.junit.Assert;


@RunWith(SpringRunner.class)
@SpringBootTest
public class Projetperso1ApplicationTests {
//	@Autowired
	
	@Test
	public void contextLoads() {
	}

	@Test
	public void testTranslation()
	{
		org.ensg.projetperso.CalculGeom maClasseTest = new CalculGeom();
		String monGeoJson = "{   \"type\": \"FeatureCollection\",   \"features\": [     {       \"type\": \"Feature\",       \"properties\": {},       \"geometry\": {         \"type\": \"LineString\",         \"coordinates\": [           [             2.552701234817505,             48.84843671561108           ],           [             2.553527355194092,             48.84854967779318           ]         ]       }     }   ] }";
		String monResAttendu = "{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"LineString\",\"coordinates\":[[2.5537,48.8496],[2.5546,48.8497]]}}]}";
		maClasseTest.setVecteur( new Coordinate(100, 100) );
		String monRes = maClasseTest.ComputeGeometriesIntoString(monGeoJson, CalculGeomEnum.Translation);
		Assert.assertTrue(monResAttendu==monRes);
	}

	
}
