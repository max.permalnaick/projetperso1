package org.ensg.projetperso;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Projetperso1Application {

	public static void main(String[] args) {
		SpringApplication.run(Projetperso1Application.class, args);
	}
}
