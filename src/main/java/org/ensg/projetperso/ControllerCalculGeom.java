package org.ensg.projetperso;

import org.locationtech.jts.geom.Coordinate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 
 * @author maxp
 * @brief : page HTML envoie la requête GET avec un paramètre string encodé en uri
 * 			SpringBoot reçoit ici le paramètre sGEOJson sous un format non uri => déjà décodé
 */
@RestController
public class ControllerCalculGeom {
	@RequestMapping(value="/api/rotation", method=RequestMethod.GET)
	String calculGeomRotation(@RequestParam(value="geom", defaultValue="N/A") String _sGEOJson,
			@RequestParam(value="angle", defaultValue="0.5") String _angle)
	{
		try {
			CalculGeom rotation = new CalculGeom();
			Double angle = Double.parseDouble(_angle);
			rotation.setvaleur(angle);
			String val = rotation.ComputeGeometriesIntoString(_sGEOJson, CalculGeomEnum.Rotation);
			return val;
		}
		catch(Exception e)
		{
			return "Erreur dans le programme API REST";
		}
	}

	@RequestMapping(value="/api/translation", method=RequestMethod.GET)
	String calculGeomTranslation(@RequestParam(value="geom", defaultValue="N/A") String _sGEOJson,
			@RequestParam(value="x", defaultValue="100") String _x,
			@RequestParam(value="y", defaultValue="100") String _y)
	{
		try {
			CalculGeom translation = new CalculGeom();
			Double x = Double.parseDouble(_x);
			Double y = Double.parseDouble(_y);
			translation.setVecteur(new Coordinate(x, y));
			String val = translation.ComputeGeometriesIntoString(_sGEOJson, CalculGeomEnum.Translation);
			return val;
		}
		catch(Exception e)
		{
			return "Erreur dans le programme API REST";
		}
	}
}
