/**
 * Les fonctions JavaScript pour l'IHM test de l'API REST
 * @author Max
 */





/**
 * Les variables globales de l'IHM
 * @url https://leafletjs.com/examples/geojson/
 */
var G_nbOperation=0;			/// permet de comptabiliser le nombre d'opération
var G_myStyleIn = {				/// style par défaut d'une géométrie chargée sans tranformation
		"color": "#000078",
		"weight": 5,
		"opacity": 0.65
};
var G_myStyleOut = {			/// style par défaut d'une géométrie après tranformation
		"color": "#ff7800",
		"weight": 5,
		"opacity": 0.65
};
var G_JsonStr;					/// la chaine URI de la géométrie chargée
var G_mymap;					/// la map de leaflet






/////////////////////////////////////////////////////////////////////////////
//ENCODAGE/DECODAGE 
/////////////////////////////////////////////////////////////////////////////
function jsonToURI(json){ return encodeURIComponent(JSON.stringify(json)); }
function uriToJSON(urijson){ return JSON.parse(decodeURIComponent(urijson)); }






///////////////////////////////////////////////////////////////////////////////
//AJAX : encapsulation d'une requete ajax dans une promesse
///////////////////////////////////////////////////////////////////////////////
/**
 * encapsulation d'une requete ajax dans une promesse
 * @param url
 * @param method
 * @param data
 */
function ajax(url, method, data)
{
	return new Promise( function(resolve, reject)
	{
		var request = new XMLHttpRequest();
		request.open(method, url, true);

		if (!(data instanceof FormData)) {
			request.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
		}

		request.onreadystatechange = function()
		{
			if (request.readyState === XMLHttpRequest.DONE)
			{
				if (request.status === 200) {
					resolve(request.responseText);
				}
				else {
					reject(Error(request.statusText));
				}
			}
		};

		request.onerror = function() {
			reject(Error("Network Error"));
		};

		request.send(data);
	});
}






/////////////////////////////////////////////////////////////////////////////
//UPLOAD FILE AND PRINT RESULT : crée callback évènement sur élément html
/////////////////////////////////////////////////////////////////////////////
/**
 * Crée un callback évènement sur élément html
 * @url https://openclassrooms.com/forum/sujet/recuperer-le-contenu-d-un-fichier-local-35912
 */
function initWindow()
{
	window.onload = function()
	{
		var f   = document.getElementById('file');
		var res = document.getElementById('resultat');
		var info = document.getElementById('info');
	
		f.onchange = function()
		{
			var file = f.files[0];
			var fr   = new FileReader();
	
			fr.onprogress = function() {
				info.innerHTML = 'Chargement...';
			};
	
			fr.onerror = function() {
				info.innerHTML = 'Oups, une erreur s\'est produite...';
			};
	
			fr.onload = function()
			{
				G_JsonStr="";
				try
				{
					console.log("Chargement du résultat dans une chaine");
					info.innerHTML = '';
					var sRes = fr.result;
					var json = JSON.parse(sRes);
					var jsonStr = JSON.stringify(json);
					var sURI = encodeURIComponent(jsonStr);
					G_JsonStr = sURI;
					info.innerHTML = 'fichier chargé : ' + jsonStr + '\nencodé : ' + sURI;
					console.log("Fin encodage de la chaine en URI");
					L_fitBoundMap(json,G_myStyleIn);
	
				}
				catch (e) {
					console.error("Erreur dans le parsing du fichier :", e); 
					info.innerHTML = 'Erreur dans le parsing du fichier : '+ e;
				}
			};
	
			fr.readAsText(file);
		};
	};
}












/**
 * Initialise le contexte leaflet
 * @url https://leafletjs.com/examples/quick-start/
 */
function initLeaflet()
{
	G_mymap = L.map('mapid').setView([ 51.505, -0.09 ], 13);
	L.tileLayer(
					'https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw',
					{
						maxZoom : 18,
						attribution : 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, '
								+ '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, '
								+ 'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
						id : 'mapbox.streets'
					}).addTo(G_mymap);
}






/**
 * Formate et envoie la requête au REST, les fonctions DataGetAngle et DataGetVecteur
 * permettent de récupérer les valeurs entrées pour les opérations
 * @param _operation le type d'opération à effectuer : rotation ou translation
 * @param _params les paramètres éventuels à ajouter
 */
function SendDataToApiREST(_operation,_params)
{
	G_nbOperation++;

	var info = document.getElementById('info');
	var tsk = document.getElementById('tache');
	var res = document.getElementById('resultat');
	var id = G_nbOperation +' ' + _operation;

	tsk.appendChild(document.createTextNode( id + '\n' ));
	var promise = ajax('http://localhost:8080/api/'+_operation+'?geom=' + G_JsonStr+_params, "GET", null);
	promise.then(
			//promise tenue
			function(_resultat)
			{
				try{
					res.appendChild(document.createTextNode( id + ' : ' + _resultat + '\n' ));
					var json = JSON.parse(_resultat);
					L_fitBoundMap(json,G_myStyleOut);
				}
				catch (e) {
					console.error("Erreur dans le parsing du fichier :", e); 
					info.innerHTML = 'Erreur dans le parsing du fichier : '+ e;
				}
			},
			//promise rejetée
			function(_resultatRejete)
			{
//				res.appendChild(document.createTextNode( id + ' : ' + _resultatRejete + '\n' ));
				res.appendChild(document.createTextNode( id + ' : ' + "ERREUR La page demandée n'existe pas, seules les interfaces .../api/translation et .../api/rotation sont accessibles." + '\n' ));
			});
}
function DataGetAngle()
{
	var angle = document.getElementById('angleRotation');
	var valueAsNumber = angle.valueAsNumber * 3.14 / 180.0;
	var sURI = encodeURIComponent(valueAsNumber);
	return "&angle="+sURI;
}
function DataGetVecteur()
{
	var tX = document.getElementById('tX');
	var tY = document.getElementById('tY');
	var sURIx = encodeURIComponent(tX.valueAsNumber);
	var sURIy = encodeURIComponent(tY.valueAsNumber);
	return "&x="+sURIx+"&y="+sURIy;
}






/**
 * Cette fonction permet de zoomer sur la géométrie ajoutée dans leaflet
 * @url https://esri.github.io/esri-leaflet/examples/zooming-to-all-features-2.html
 * @url https://leafletjs.com/examples/geojson/
 * @url https://gis.stackexchange.com/questions/166863/how-to-calculate-the-bounding-box-of-a-geojson-object-using-python-or-javascript
 * @param _json
 * @param _style
 */
function L_fitBoundMap(_json,_style)
{
	var bounds = L.latLngBounds([]);
	var myGeoJSON = L.geoJson(_json, {
		style: _style,
	    onEachFeature: function (feature, layer)
	    {
	        // récupère les bornes
	        feature.properties.bounds_calculated = layer.getBounds();
	        bounds.extend(layer.getBounds());
	    }
	}).addTo(G_mymap);
	G_mymap.fitBounds(bounds);
}
