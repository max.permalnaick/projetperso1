package org.ensg.projetperso;

import java.awt.geom.AffineTransform;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;

import org.geotools.feature.FeatureIterator;
import org.geotools.geojson.feature.FeatureJSON;
import org.geotools.geometry.jts.JTS;
import org.geotools.referencing.CRS;
import org.geotools.referencing.crs.DefaultGeographicCRS;
import org.geotools.referencing.operation.transform.AffineTransform2D;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryCollection;
import org.locationtech.jts.geom.Point;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.geometry.MismatchedDimensionException;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.opengis.referencing.operation.MathTransform;
import org.opengis.referencing.operation.TransformException;

import io.swagger.annotations.ApiOperation;







/**
 * 
 * @author Max
 *
 */
public class CalculGeom {
	private double mValeur = 0.0;
	private Coordinate mVecteur = new Coordinate(0,0);
	private ArrayList<Geometry> resListGeometriesWGS84 = null;
	private ArrayList<Geometry> mListGeometries =  null;





	/**
	 * Cette méthode parse le string passé en paramètre, ce string doit être un fichier geojson chargé.
	 * Autrement une exception sera levée.
	 * La méthode permet de charger un fichier geojson que ce soit une collection de features, feature, collection
	 * de geometries, ou une geometry.
	 * Elle renseigne directement l'attribut ArrayList<Geometry> de la classe 
	 * @url https://tools.ietf.org/html/rfc7946#section-4
	 * @url http://docs.geotools.org/stable/javadocs/org/geotools/geojson/feature/FeatureJSON.html
	 * @url http://docs.geotools.org/latest/javadocs/org/geotools/geojson/geom/GeometryJSON.html
	 * @url http://docs.geotools.org/stable/javadocs/
	 * @url https://locationtech.github.io/jts/javadoc/org/locationtech/jts/geom/Geometry.html
	 * @url http://docs.geotools.org/latest/userguide/unsupported/geometry/operation.html
	 * @url http://turfjs.org/docs#tesselate
	 * @param _string fichier geojson chargé
	 * @throws IOException
	 */
	@ApiOperation(value = "Cette méthode parse le string passé en paramètre, ce string doit être un fichier geojson chargé",
			    notes = "La méthode permet de charger un fichier geojson que ce soit une collection de features, feature, collection\n" + 
			    		"de geometries, ou une geometry.\n" + 
			    		"Elle renseigne directement l'attribut ArrayList<Geometry> de la classe ")
	private ArrayList<Geometry> GetGeometriesList(String _string) throws IOException
	{
		ArrayList<Geometry> listGeoms = null;
		listGeoms = new ArrayList<Geometry>();

		// est ce une simple feature?	
		FeatureJSON fjsonReader = new FeatureJSON();
		SimpleFeature feature = fjsonReader.readFeature(_string);
		if(feature!=null)
		{
			Geometry geom = (Geometry) feature.getDefaultGeometry();
			if(geom!=null) {
				listGeoms.add(geom);
				return listGeoms;
			}
		}


		//  une collection de features ?
		FeatureIterator<SimpleFeature> features = fjsonReader.streamFeatureCollection(_string);
		while (features.hasNext())
		{
			feature = features.next();
			Geometry geom = (Geometry) feature.getDefaultGeometry();
			if(geom==null) continue;
			listGeoms.add(geom);
		}
		if(listGeoms.size()>0)
			return listGeoms;


		// une géométrie ?
		org.geotools.geojson.geom.GeometryJSON gjson = new org.geotools.geojson.geom.GeometryJSON();
		Geometry geom = gjson.read(_string);
		if(geom!=null) {
			listGeoms.add(geom);
			return listGeoms;
		}


		// ou une collection de géométrie?
		GeometryCollection geometries = gjson.readGeometryCollection(_string);
		for(int i=0 ;i< geometries.getNumGeometries(); i++)
		{
			listGeoms.add(geometries.getGeometryN(i));
		}
		return listGeoms;
	}

	/**
	 * @url http://docs.geotools.org/stable/userguide/library/referencing/crs.html
	 * @url http://docs.geotools.org/stable/userguide/library/api/jts.html
	 * @return
	 * @throws FactoryException
	 * @throws MismatchedDimensionException
	 * @throws TransformException
	 */
	private void transformRotationTranslation(CalculGeomEnum _transformation) throws FactoryException, MismatchedDimensionException, TransformException, Exception
	{

		//https://github.com/geotools/geotools/blob/master/modules/library/referencing/src/test/resources/org/geotools/referencing/test-data/scripts/CylindricalEqualArea.txt
		String WKT = "PROJCS[\"World_Behrmann\",GEOGCS[\"GCS_WGS_1984\",DATUM[\"WGS_1984\",SPHEROID[\"WGS_1984\",6378137,298.257223563]],PRIMEM[\"Greenwich\",0],UNIT[\"Degree\",0.017453292519943295]],PROJECTION[\"Behrmann\"],PARAMETER[\"False_Easting\",0],PARAMETER[\"False_Northing\",0],PARAMETER[\"Central_Meridian\",0],UNIT[\"Meter\",1],AUTHORITY[\"EPSG\",\"54017\"]]";

		resListGeometriesWGS84 = null;
		resListGeometriesWGS84 = new ArrayList<Geometry>();
		
		CoordinateReferenceSystem crs = CRS.parseWKT(WKT);
		MathTransform transform = CRS.findMathTransform(DefaultGeographicCRS.WGS84, crs);
		for(int i=0; i<mListGeometries.size() ; i++)
		{
			// wgs84 -> mondiale
			Geometry geomtarget = JTS.transform(mListGeometries.get(i), transform);

			AffineTransform affineTransform = null;
			if( _transformation == CalculGeomEnum.Rotation )
			{
				Point ancorPoint = geomtarget.getCentroid();
				affineTransform = AffineTransform.getRotateInstance(mValeur, ancorPoint.getX(), ancorPoint.getY());
			}
			else if(_transformation == CalculGeomEnum.Translation )
				affineTransform = AffineTransform.getTranslateInstance(mVecteur.x, mVecteur.y);
			else
				throw new Exception("Calcul non pris en compte");
			MathTransform mathTransform = new AffineTransform2D(affineTransform);

			// mondiale -> wgs84
			geomtarget = JTS.transform(geomtarget, mathTransform);		
			geomtarget = JTS.transform(geomtarget, CRS.findMathTransform(crs,DefaultGeographicCRS.WGS84));
			resListGeometriesWGS84.add(geomtarget);
		}
	}

	/**
	 * 
	 * @param _list
	 * @return
	 * @throws IOException
	 */
	private String writerInString(ArrayList<?> _list) throws IOException
	{
		String HEADER="{ \"type\": \"FeatureCollection\", \"features\": [ ";
		String FOOTER = "] }";
		String FEATURE_HEADER="{ \"type\": \"Feature\", ";
		String FEATURE_FOOTER="} ";
		String PROPER_HEADER="\"properties\": {";
		String PROPER_FOOTER="}, ";
		String GEOM_HEADER = "\"geometry\": ";
		String GEOM_FOOTER = " ";
		
		Writer output = new StringWriter();
		if(_list.get(0) instanceof Geometry )
		{
			output.write(HEADER);
			org.geotools.geojson.geom.GeometryJSON gjson = new org.geotools.geojson.geom.GeometryJSON();
			for(int i=0; i<_list.size() ; i++)
			{
				if(i>0) output.write(", ");
				
				output.write(FEATURE_HEADER);
				output.write(PROPER_HEADER+PROPER_FOOTER);
				output.write(GEOM_HEADER);
				Geometry geom = (Geometry) _list.get(i);
				gjson.write(geom, output);
				output.write(GEOM_FOOTER);
				output.write(FEATURE_FOOTER);
			}
			output.write(FOOTER);
			return output.toString();
		}
		else if(_list.get(0) instanceof Double )
		{
			output.write("{");
			for(int i=0; i<_list.size() ; i++)
			{
				if(i>0) output.write(", ");
				Double val = (Double) _list.get(i);
				output.write( "\"valeur"+i+"\": \"" + val + "\"");
			}
			output.write("}");
			return output.toString();
		}
		return "";
	}
	
	/**
	 * 
	 * @url CRS.findMathTransform : http://docs.geotools.org/stable/userguide/tutorial/geometry/geometrycrs.html
	 * @url http://docs.geotools.org/stable/userguide/library/referencing/crs.html//
	 * @url http://docs.geotools.org/stable/userguide/library/jts/geometry.html
	 * @param _string
	 * @param _calculateur
	 * @return
	 */
	public String ComputeGeometriesIntoString(String _string, CalculGeomEnum _operation)
	{
		try 
		{
			String resultat="";
			mListGeometries = GetGeometriesList(_string);
			transformRotationTranslation(_operation);
			resultat=writerInString(resListGeometriesWGS84);
			return resultat;
			
		}catch (IOException e){
			e.printStackTrace();
			return "ERREUR le fichier geojson n'est pas lisible par GEOTOOLS";
		}catch ( FactoryException e){
			e.printStackTrace();
			return "ERREUR dans la projection";
		}catch ( MismatchedDimensionException e){
			e.printStackTrace();
			return "ERREUR dans la transformation de la géométrie";
		}catch ( Exception e){
			e.printStackTrace();
			return "ERREUR dans le calcul de la géométrie";
		} 
	}

	/**
	 * @param mValeur the mValeur to set
	 */
	public void setvaleur(double mValeur) {
		this.mValeur = mValeur;
	}

	/**
	 * @param mVecteur the mVecteur to set
	 */
	public void setVecteur(Coordinate mVecteur) {
		this.mVecteur = mVecteur;
	}
}
